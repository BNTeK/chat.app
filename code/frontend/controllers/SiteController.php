<?php
namespace frontend\controllers;

use common\models\Access;
use common\models\Message;
use common\models\Room;
use Faker\Provider\cs_CZ\DateTime;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'room'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'room'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $access = Access::find()->where(['user_id' => Yii::$app->user->id])->asArray()->all();
        $access = ArrayHelper::getColumn($access, 'room_id');

        $rooms = Room::find()->where('id =:room_id OR type = :type', ['room_id' => $access, 'type' => 0])->all();

        return $this->render('index', ['rooms' => $rooms]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionRoom($room_id)
    {
        $is_public = Room::find()->where(['id' => $room_id, 'type' => '0'])->one();

        if (empty($is_public)) {

            $access = Access::find()->where(['user_id' => Yii::$app->user->id, 'room_id' => $room_id])->one();
            $room = Room::find()->where(['id' => $room_id])->one();
            If (!empty($access)) {
                return $this->render('room', [
                    'room' => $room]);

            } else {
                throw new ForbiddenHttpException("Доступ запрещен!");
            }

        } else {
            return $this->render('room', [
                'room' => $is_public]);
        }


    }

    public function actionHistory($room_id)
    {
        if (Yii::$app->getRequest()->getIsAjax()) {

            $message = Message::find()->where(['room_id' => $room_id])->with('user')->asArray()->all();

            ArrayHelper::multisort($message, 'created_at', SORT_ASC);

            return Json::encode($message);

        }
    }

    public function actionMessage($room_id)
    {
        if (Yii::$app->getRequest()->getIsAjax()) {

            $last_message_time = Json::encode(Yii::$app->getRequest()->post()['last_message_time']);
            $last_message_time = substr(substr($last_message_time, 1), 0, -1);


            $message = Message::find()->where('room_id = :room_id AND created_at > :last_message_time', [':room_id' => $room_id, ':last_message_time' => $last_message_time])->with('user')->asArray()->all();
            ArrayHelper::multisort($message, 'created_at', SORT_ASC);


            return Json::encode($message);
        }
    }

    public function actionSend($room_id)
    {
        if (Yii::$app->getRequest()->getIsAjax()) {
            if (Yii::$app->getRequest()->post()) {
                $new = new Message();

                $new->room_id = $room_id;
                $new->text = Yii::$app->getRequest()->post()['message'];
                $new->user_id = Yii::$app->user->identity->id;
                $new->room_id = $room_id;
                $new->save();
            }

            return Json::encode(Yii::$app->request->post());
        }
    }
}
