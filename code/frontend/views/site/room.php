<?php

use \yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Chat.app - Комнатa -'. $room->name;
?>
<div class="site-index">

    <div class="well">
        <h2 class="text-center">Канал "<?=  $room->name; ?>".</h2>
    </div>
    <hr>
    <div class="col-md-12">

        <div class="col-md-12 well" id="chat_box"> </div>
        <div class="input-group">
            <input type="text" class="form-control" id="message_field" >
            <div class="input-group-btn">
                <button type="button" class="btn btn-default" onClick="send()">Отправить</button>
            </div>
        </div>

    </div>

</div>

<?php
$this->registerJs("

$(document).keypress(function(e) {
    if(e.which == 13) {
        send();
    }
});

var last_message_time = '';

$.ajax({
  url: '".Url::to(['site/history','room_id' => $room->id],true)."',
  success: function(data) {
    data = JSON.parse(data);
    last_message_time = data[Object.keys(data).length-1].created_at;
    $.each(data, function(index, value) {
       $('#chat_box').html($('#chat_box').html() + '<p>' + value.created_at + ' ' + '<strong>' + value.user.username +'</strong>' +':' + value.text + '</p>');
    });
  }
})

window.setInterval(function(){
 var data = {'last_message_time':last_message_time};
$.ajax({
  type: 'POST',
  url: '".Url::to(['site/message','room_id' => $room->id],true)."',
  data: data,
  success: function(data) {
    data = JSON.parse(data);
    last_message_time = data[Object.keys(data).length-1].created_at;
    $.each(data, function(index, value) {
        $('#chat_box').html($('#chat_box').html() + '<p>' + value.created_at + ' ' + '<strong>' + value.user.username +'</strong>' +':' + value.text + '</p>');
    });
  }
})
}, 1000);

send = (function () {
var message = $('#message_field').val();
var data = {'message': message}
$.ajax({
type: 'POST',
url: '".Url::to(['site/send','room_id' => $room->id],true)."',
data: data,
  success: function(data) {

  },
error: function(e) {
    console.log(e);
}
});
});
");
?>





