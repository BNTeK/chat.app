<?php
/* @var $this yii\web\View */
$this->title = 'Chat.app - Комнаты';
?>
<div class="site-index">

    <div class="well">
        <h2 class="text-center">Привет, <?= Yii::$app->user->identity->username ?></h2>

        <h3 class="text-center">Тебе доступны следующие каналы!</h3>
    </div>

    <hr>

    <div class="well col-md-12">

        <!--        Public room start-->
        <div class="col-md-8 col-md-offset-2">
            <h2 class="text-center">Публичные комнаты</h2>
            <?php foreach ($rooms as $room): ?>
                <?php if ($room->type == 0): ?>
                    <a href="<?= \yii\helpers\Url::to(['site/room', 'room_id' => $room->id])?>">
                        <div class="col-md-4">
                            <p><?= $room->name ?></p>
                        </div>
                    </a>
                <?php endif ?>
            <?php endforeach ?>
        </div>
        <!--        Public room end-->
        <!--        Private room start-->
        <div class="col-md-8 col-md-offset-2">
            <h2 class="text-center">Закрытые комнаты</h2>
            <?php foreach ($rooms as $room): ?>
                <?php if ($room->type == 1): ?>
                    <a href="<?= \yii\helpers\Url::to(['site/room', 'room_id' => $room->id])?>">
                        <div class="col-md-4">
                            <p><?= $room->name ?></p>
                        </div>
                    </a>
                <?php endif ?>
            <?php endforeach ?>
        </div>
        <!--        Private room end-->
    </div>

</div>
