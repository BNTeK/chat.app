<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "room".
 *
 * @property integer $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property integer $type
 *
 * @property Access[] $accesses
 */
class Room extends \yii\db\ActiveRecord
{
    public $userIds = [];

    const TYPE_PUBLIC = 0;
    const TYPE_PRIVATE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['created_at', 'updated_at', 'userIds'], 'safe'],
            [['type'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название комнаты',
            'created_at' => 'Созданa в',
            'updated_at' => 'Обновленa в',
            'type' => 'Тип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccesses()
    {
        return $this->hasMany(Access::className(), ['room_id' => 'id']);
    }

    public function getTypeList()
    {
        return array(
            self::TYPE_PRIVATE  => 'Закрытая',
            self::TYPE_PUBLIC => 'Публичная',
        );
    }

    public function getAllUsers()
    {
        $data = User::find()->where(['status' => 10])->asArray()->all();
        return ArrayHelper::map($data, 'id', 'username');
    }



    public function afterSave($insert, $changedAttributes)
    {

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                if (!empty($this->userIds)) {
                    foreach($this->userIds as $id)
                    {
                        $acceess = new Access();
                        $acceess->user_id = $id;
                        $acceess->room_id = $this->id;
                        $acceess->save();
                        }
                    $transaction->commit();
                }

            } catch(Exception $e) {
                $transaction->rollback();
            }

            return true;

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeSave($insert)
    {
        if(!$this->isNewRecord)
        {
            if (parent::beforeSave($insert)) {
                Access::deleteAll(['room_id' => $this->id]);
                return true;
            } else {
                return false;
            }
        }
    }
}
