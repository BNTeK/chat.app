<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $created_at
 * @property integer $room_id
 * @property string $text
 *
 * @property User $user
 */
class Message extends \yii\db\ActiveRecord
{
    public $updated_at;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'room_id'], 'required'],
            [['user_id', 'room_id'], 'integer'],
            [['created_at','updated_at'], 'safe'],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'created_at' => 'Создано в',
            'room_id' => 'Комната',
            'text' => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);

    }

    public function getAllUsers()
    {
        $data = User::find()->where(['status' => 10])->asArray()->all();
        return ArrayHelper::map($data, 'id', 'username');
    }

    public function getAllRooms()
    {
        $data = Room::find()->asArray()->all();
        return ArrayHelper::map($data, 'id', 'name');
    }
}
