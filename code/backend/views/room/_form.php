<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Room */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="room-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'type')->dropDownList($model->getTypeList(),['id' => 'type_field']) ?>


    <?= $form->field($model, 'userIds')->widget(Select2::classname(), [
            'data' => $model->getAllUsers(),
            'language' => 'ru',
            'value' => $model->userIds , // value to initialize
            'options' => ['multiple' => true, 'placeholder' => 'Select states ...'],
            'pluginOptions' => [
            'allowClear' => true
            ],
        ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$this->registerJs("


$('#type_field').change(function()
{
    if($('#type_field').val() == 1){
        $('.field-room-userids').show();
    }else{
        $('.field-room-userids').hide();
        }
});

$( document ).ready(function() {

    if($('#type_field').val() == 1){
        $('.field-room-userids').show();
    }else{
        $('.field-room-userids').hide();
        }
});
");
?>
