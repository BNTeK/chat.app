<?php

use yii\db\Schema;
use yii\db\Migration;

class m150722_180617_access_base extends Migration
{
    public function up()
    {
        $this->createTable('{{%access}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'room_id' =>Schema::TYPE_INTEGER . ' NOT NULL',

        ]);

        $this->addForeignKey(
            'fk_access_user_id',
            '{{%access}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_access_room_id',
            '{{%access}}',
            'room_id',
            '{{%room}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%access}}');
    }
    

}
