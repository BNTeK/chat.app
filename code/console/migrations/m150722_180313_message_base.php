<?php

use yii\db\Schema;
use yii\db\Migration;

class m150722_180313_message_base extends Migration
{
    public function up()
    {
        $this->createTable('{{%message}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT 0',
            'room_id' =>Schema::TYPE_INTEGER . ' NOT NULL',
            'text' => Schema::TYPE_TEXT,

        ]);

        $this->addForeignKey(
            'fk_message_user_id',
            '{{%message}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%message}}');
    }
    

}
