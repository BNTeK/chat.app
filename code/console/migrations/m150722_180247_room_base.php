<?php

use yii\db\Schema;
use yii\db\Migration;

class m150722_180247_room_base extends Migration
{
    public function up()
    {
        $this->createTable('{{%room}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT 0',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT 0',
            'type' =>Schema::TYPE_INTEGER

        ]);

    }

    public function down()
    {
        $this->dropTable('{{%room}}');
    }

}
